import zmq
import sys
import time

port = '5556'
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

counter = 0
while True:
    topic = "stdout"
    messageData = "standard out message %d" % counter
    print "Sending %s %s" % (topic, messageData)
    socket.send("%s %s" % (topic, messageData))
    counter += 1
    time.sleep(0.1)

"""
import zmq

port = "5556"
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://localhost:%s" % port)
topicfilter = "stdout"
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

while True:
    string = socket.recv()
    print string
"""