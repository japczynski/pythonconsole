import os
import sys
from code import InteractiveConsole
import StringIO
import Queue
import time
import signal
import threading
import ctypes

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import uic
from PyQt4.Qsci import QsciScintilla
from PyQt4.Qsci import QsciLexerPython
from PyQt4 import QtCore

# class Shell(InteractiveConsole):
#     def write(self, data):
#         sys.stdout.write(data)


class Tee(QObject):
    writing = pyqtSignal(str)
    
    def __init__(self, stream):
        QObject.__init__(self)
        self.stream = stream
        self.use_stream = self.stream.isatty()
    
    def write(self, data):
        self.writing.emit(data)
        if self.use_stream:
            self.stream.write(data)


def _async_raise(tid, excobj):
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid,
                                                     ctypes.py_object(excobj))
    if res == 0:
        raise ValueError("nonexistent thread id")
    elif res > 1:
        # """if it returns a number greater than one, you're in trouble, 
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
        raise SystemError("PyThreadState_SetAsyncExc failed")


class ThreadMDK(SystemExit):
    pass


class WorkerThread(threading.Thread):
    def raise_exc(self, excobj):
        assert self.isAlive(), "thread must be started"
        for tid, tobj in threading._active.items():
            if tobj is self:
                _async_raise(tid, excobj)
                return
        
        # the thread was alive when we entered the loop, but was not found 
        # in the dict, hence it must have been already terminated. should we raise
        # an exception here? silently ignore?
    
    def mdk(self):
        # must raise the SystemExit type, instead of a SystemExit() instance
        # due to a bug in PyThreadState_SetAsyncExc
        self.raise_exc(ThreadMDK)


class PythonConsole(QMainWindow):
    def __init__(self, parent=None, namespace=None):
        QMainWindow.__init__(self, parent)
        uic.loadUi('PythonConsole.ui', self)
        
        if not namespace:
            namespace = locals()
        # self.__shell = Shell(namespace)
        self.__shell = InteractiveConsole(namespace)
        
        self.configureScintilla()
        
        self.__tee = Tee(sys.stdout)
        self.__stdout = sys.stdout
        sys.stdout = self.__tee
        self.__tee.writing.connect(self.writeOutput)
        
        self.__teeErr = Tee(sys.stderr)
        self.__stderr = sys.stderr
        sys.stderr = self.__teeErr
        self.__teeErr.writing.connect(self.writeOutput)
        
        sequence = QKeySequence("Ctrl+Return")
        ctrlenter = QShortcut(self.inputTextEdit)
        ctrlenter.setKey(sequence)
        ctrlenter.activated.connect(self.execute)
        
        sequence = QKeySequence("F12")
        breakKey = QShortcut(self.inputTextEdit)
        breakKey.setKey(sequence)
        breakKey.activated.connect(self.breakThread)
        
        sequence = QKeySequence("Ctrl+s")
        saveKey = QShortcut(self.inputTextEdit)
        saveKey.setKey(sequence)
        saveKey.activated.connect(self.saveCode)
        
        self.__resolvePrivateNames = True
        self.actionResolveSelf.toggled.connect(self.setResolveSelf)
        
        self.__runningCodeLock = threading.Lock()
        with self.__runningCodeLock:
            self.__runningCode = False
        
        if os.path.exists('PythonConsoleCode.py'):
            text = None
            with open('PythonConsoleCode.py', 'r') as codeFile:
                text = codeFile.read()
            self.inputTextEdit.setText(text)
    
    def setResolveSelf(self, checked):
        self.__resolvePrivateNames = checked
    
    def breakThread(self):
        self.__worker.mdk()
    
    def setCodeRunning(self, running):
        self.__runningCode = running
        if self.__runningCode:
            self.progressBar.setMaximum(0)
        else:
            self.progressBar.setMaximum(1)
    
    def saveCode(self):
        with open('PythonConsoleCode.py', 'w') as codeFile:
            codeFile.write(str(self.inputTextEdit.text()))
    
    def closeEvent(self, event):
        sys.stdout = self.__stdout
        QWidget.closeEvent(self, event)
    
    def writeOutput(self, data):
        self.outputTextEdit.append(data)
        outputlines = self.outputTextEdit.lines()
        self.outputTextEdit.ensureLineVisible(outputlines - 1)
    
    def configureScintilla(self):
        self.outputTextEdit.setReadOnly(True)
        
        font = QFont()
        font.setFamily('Courier')
        font.setFixedPitch(True)
        font.setPointSize(10)
        fontMetrics = QFontMetrics(font)
        
        self.outputTextEdit.setFont(font)
        self.inputTextEdit.setFont(font)
        
        lexer = QsciLexerPython()
        lexer.setDefaultFont(font)
        self.inputTextEdit.setLexer(lexer)
        
        self.inputTextEdit.setMarginsFont(font)
        self.inputTextEdit.setMarginWidth(0, fontMetrics.width("0000") + 6)
        self.inputTextEdit.setMarginLineNumbers(0, True)
        self.inputTextEdit.setMarginsBackgroundColor(QColor("#cccccc"))
        self.inputTextEdit.setFolding(QsciScintilla.BoxedTreeFoldStyle)
        self.inputTextEdit.setIndentationsUseTabs(False)
        self.inputTextEdit.setIndentationWidth(4)
        self.inputTextEdit.setAutoIndent(True)
        self.inputTextEdit.setWhitespaceVisibility(QsciScintilla.WsVisible)
        self.inputTextEdit.setEdgeMode(QsciScintilla.EdgeLine)
        self.inputTextEdit.setEdgeColumn(80)
        self.inputTextEdit.setBraceMatching(QsciScintilla.SloppyBraceMatch)
        self.inputTextEdit.setBackspaceUnindents(True)
        self.inputTextEdit.setIndentationGuides(True)
        self.inputTextEdit.setEolMode(QsciScintilla.EolUnix)
        
        self.inputTextEdit.setFocus()
    
    def workFunc(self, code):
        with self.__runningCodeLock:
            self.setCodeRunning(True)
        try:
            if self.__resolvePrivateNames:
                selfObject = self.__shell.locals.get('self')
                if selfObject:
                    objectName = selfObject.__class__.__name__
                    code = code.replace('self.__', 'self._%s__' % objectName)
            self.__shell.runcode(code)
        except ThreadMDK:
            sys.stdout.write('\n* * * * BREAK * * * *\n')
        finally:
            with self.__runningCodeLock:
                self.setCodeRunning(False)
    
    def execute(self):
        if not self.__runningCode:
            code = self.inputTextEdit.selectedText()
            if not code:
                code = self.inputTextEdit.text()
            self.__worker = WorkerThread(target=self.workFunc,
                                       args=(str(code),))
            self.__worker.daemon = True
            self.__worker.start()

if __name__ == '__main__':
    # if not sys.stdout.isatty():
        # sys.stdout.close()
        # stdout = open('stdout.txt', 'w')
        
        # sys.stdout = stdout
        # sys.__stdout__ = sys.stdout
    
    app = QApplication(sys.argv)
    win = PythonConsole()
    win.show()
    sys.exit(app.exec_())